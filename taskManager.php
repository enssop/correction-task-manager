<?php

$typePossiblesDAction = ['aide', 'a', 'tache', 't', 'supprimer', 's', 'liste', 'l', 'quitter', 'q'];

$listeDesTaches = [];
$idProchaineTache = 1;

$executionEnCours = true;

function afficherTexte($texte, $couleur) {
    echo "\033[";
    switch ($couleur) {
        case 'red':
            echo "0;31";
            break;
        case 'green':
            echo "0;32";
            break;
    }
    echo "m";
    echo $texte;
    echo "\033[0m";
}

function idExiste($id) {
    global $listeDesTaches;

    foreach ($listeDesTaches AS $tache ) {
        if ( $tache['id'] == $id ) {
            return true;
        }
    }

    return false;
}

do {

    do {
        afficherTexte("Que souhaitez vous faire ? (" . implode(", ", $typePossiblesDAction) . ")\n", "red");
        echo " > ";
        $typeDAction = trim(fgets(STDIN));
    } while (!in_array($typeDAction, $typePossiblesDAction));

    switch ($typeDAction) {
        case 'aide':
        case 'a':
            echo "de l'aide\n";
            break;
        case 'tache':
        case 't':
            afficherTexte("Veuillez entrer le nom de la tache :\n > ", "green");
            $nomTache = trim(fgets(STDIN));
            $listeDesTaches[] = [
                'id' => $idProchaineTache,
                'nom' => $nomTache,
            ];
            $idProchaineTache++;
            break;
        case 'supprimer':
        case 's':
            foreach ($listeDesTaches as $tache) {
                echo $tache['nom'] . " (" . $tache['id'] . ")\n";
            }

            do {
                echo "Quelle tache supprimer ?\n";
                $idASupprimer = intval(fgets(STDIN));
            } while( !idExiste($idASupprimer) );

            $nouveauTableau = [];
            foreach ( $listeDesTaches AS $tache ) {
                if ( $idASupprimer != $tache['id'] ) {
                    $nouveauTableau[] = $tache;
                }
            }
            $listeDesTaches = $nouveauTableau;

        break;
        case 'liste':
        case 'l':
            foreach ($listeDesTaches as $tache) {
                echo $tache['nom'] . " (" . $tache['id'] . ")\n";
            }
            break;
        case 'quitter':
        case 'q':
            $executionEnCours = false;
            break;
    }

} while ($executionEnCours);
